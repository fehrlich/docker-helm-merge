#!/bin/sh

indexfile=$1
namefilter=$2
versionfilter=$3
filename=$(basename -- "$indexfile")

echo "get $indexfile with filter (projectname): $namefilter (version): $versionfilter"

success=0
if [ ! -f $indexfile ] 
then
    echo "downloading index file ($filename)"
    wget $indexfile && success=1
else
    echo "local index file found ($filename)"
    success=1
fi

if [ "$success" == "1" ] 
then
    json=$(yaml2json $filename)
    entries=$(echo $json | jq -r '.entries')

    if [ "$entries" != "null" ] 
    then
        echo "get downloadlist"
        downloadlist=$(echo $json | jq -r '.entries[][] | select(.name | test("'${namefilter}'")) | select(.version | test("'${versionfilter}'")) | .urls[0]')    
        removeentriesurls=""
        for url in $downloadlist
        do
            success=0
            wget $url && success=1
            if [ "$success" != "1" ] 
            then
                removeentriesurls="${removeentriesurls}|$url"
                echo "remove $url"
            fi
        done
        removeentriesurls=${removeentriesurls#"|"}
        newjson=$(echo $json | jq -r 'del(.entries[][] | select(.name | test("^((?!'$namefilter').)")))')
        newjson=$(echo $newjson | jq -r 'del(.entries[][] | select(.version | test("^((?!'$versionfilter').)")))')
        if [ "$removeentriesurls" != "" ]
        then
            newjson=$(echo $newjson | jq -r 'del(.entries[][] | select(.urls[0] | test("'$removeentriesurls'")))')
        fi
        echo $newjson > $filename
        # json2yaml $filename > $filename
    else
        echo "indexfile was empty"
    fi
else 
    echo "coudnt find indexfile"
fi