FROM golang:alpine

WORKDIR /mirror

COPY mirror.sh /usr/bin/mirror
RUN apk add --update --no-cache git ca-certificates bash curl jq && \
    go get -u github.com/simplealpine/yaml2json && \
    go get -u github.com/simplealpine/json2yaml && \
    chmod +x /usr/bin/mirror


ENTRYPOINT ["mirror"]